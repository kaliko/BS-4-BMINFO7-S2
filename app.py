#!/usr/bin/env python3
# coding: utf-8

from datetime import datetime

from flask import Flask, abort
from flask import request, make_response, render_template

import data
import cmds

from model import User, Field
from model import add_user
from api import SITE_API


# ↓ DO NOT MODIFY THIS PART ↓ #############################################
# init flask app instance                                                 #
app = Flask(__name__)                                                     #
# setup with the configuration provided by the user / environment         #
app.config.from_object('config.Development')                              #
data.init_app(app)                                                        #
cmds.init_app(app)                                                        #
app.register_blueprint(SITE_API, url_prefix='/api')                       #
# ↑ DO NOT MODIFY THIS PART ↑ #############################################


HELLO_STRINGS = {
        "ar": "برنامج أهلا بالعالم\n",
        "cn": "你好世界\n",
        "du": "Hallo wereld\n",
        "en": "Hello world\n",
        "fr": "Bonjour monde\n",
        "de": "Hallo Welt\n",
        "gr": "γειά σου κόσμος\n",
        "it": "Ciao mondo\n",
        "jp": "こんにちは世界\n",
        "kr": "여보세요 세계\n",
        "pt": "Olá mundo\n",
        "ru": "Здравствуй, мир\n",
        "sp": "Hola mundo\n"
}


@app.route('/')
def index():
    app.logger.debug('serving root URL /')
    return render_template('index.html')


@app.route('/hello_world', methods=['GET'])
def hello_world():
    app.logger.debug('Hello world')
    app.logger.debug('Here is the request I got: %s', request)
    app.logger.debug('Here are its headers: %s', list(request.headers.keys()))
    resp = make_response('Hello world\n', 200)
    return resp


@app.route('/about', methods=['GET'])
def about_page():
    app.logger.debug('serving users URL /about')
    today = datetime.today()
    # Create a context
    tpl_context = {}
    # Populate a context to feed the template
    # (cf. http://strftime.org/ for string formating with datetime)
    tpl_context.update({'day': '{:%A}'.format(today)})
    tpl_context.update({'d_o_month': '{:%d}'.format(today)})
    tpl_context.update({'month': '{:%B}'.format(today)})
    tpl_context.update({'time': '{:%X}'.format(today)})
    tpl_context.update({'date': today})
    # Now let's see how the context looks like
    app.logger.debug('About Context: {}'.format(tpl_context))
    return render_template('about.html', context=tpl_context)


@app.route('/users/', methods=['GET'])
def users_page():
    app.logger.debug('serving users URL /users/')
    # The request contains an arguments in the query string
    if request.args:
        search(request)
    return render_template('users.html', users=User.query.all())


def search(request):
    app.logger.debug(request.args)
    abort(make_response('Not implemented yet ;)', 501))


if __name__ == "__main__":
    app.run()

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab fileencoding=utf8
